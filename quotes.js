function changeText(elemId, quotes, interval) {
  var index = 0;
  var elem = document.getElementById(elemId);
  setInterval(change, 1000 * interval);

  function change() {
    if (index == quotes.length) { index = 0; }
    elem.innerHTML = quotes[index++];
  }
}

function randomTextByMin(elemId, quotes) {
  var elem = document.getElementById(elemId);
  var index = (new Date()).getSeconds() % quotes.length;
  elem.innerHTML = quotes[index];
}

function randomText(quotes, mapValue) {
  var index = (Math.round(Math.random()*10)) % quotes.length;
  mapValue(quotes[index]);
}

function mapValue(quote) {
  var quoteBody = document.getElementById("quote");
  quoteBody.innerHTML = quote[0];

  var quoteAuthor = document.getElementById("quote_author");
  quoteAuthor.innerHTML = quote[1];
}
